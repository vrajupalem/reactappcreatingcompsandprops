import React, {Component} from 'react';
import Status from './Status'

export default class StatusList extends Component {
  
    render() {
      return (
        // renders each status in the list on to the page
        this.props.statuses.map((statusText, index) => {
          return <Status key={index} 
                         index={index} 
                         myText={statusText}
                         delete={this.props.delete} />
        })
      );
    }
  }