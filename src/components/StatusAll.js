import React, {Component} from 'react';
import StatusList from './StatusList';

import './Status.css'

export default class StatusAll extends Component {
    constructor(props) {
      super(props);
      this.state = {
        //newStatusText: 'new text',
        newStatusText: '',
        statuses: [
          "Wow! I had the most delicious lunch today!",
          "There was no traffic today!",
          "Finally finished that BIG project!",
          "Happy to be learning ReactJS!"
        ]
      }

      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.delete = this.delete.bind(this);
    }

    delete(index) {
         let copy = [...this.state.statuses];
         copy.splice(index, 1);
         this.setState({statuses: copy});
    }

    handleChange(ev) {
        //console.log("ev.target.value: ", ev.target.value)
        //debugger;
        this.setState({
            newStatusText: ev.target.value});
    }

    handleSubmit(ev) {
        // prevent the form from taking us to a new page (or refreshing)
        // Instead we will simply stay on the page
        ev.preventDefault();
        // ... is the spread operator
        let newStatuses = [this.state.newStatusText, ...this.state.statuses]
        this.setState({
            //newStatusText: 'new text',
            newStatusText: '',
            statuses: newStatuses})
    }
  
    render() {
      return (
        <div>
        <div className="status">
          <h1>Enter New Status</h1>   
          <form onSubmit={this.handleSubmit}> 
             <input type="text" value={this.state.newStatusText} 
                    onChange={this.handleChange} placeholder="What's on your mind?" />
             <button type="submit">Add Status</button>
          </form>
        </div>
        <div className="status">
          <h1>All Statuses</h1> 
          <StatusList statuses={this.state.statuses}
                      delete={this.delete} />
        </div>
        </div>
      );
    }
  }