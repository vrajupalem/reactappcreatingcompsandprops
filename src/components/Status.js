import React, {Component} from 'react';

export default class Status extends Component {
    constructor() {
      super();
      this.state = {
        likes: 0
      }
      // this is binded to the class Status
      // otherwise, JS treats this as a global and current context is used 
      // and that may not be Status class instance. This bind makes it more clear.
      this.like = this.like.bind(this);
      this.delete = this.delete.bind(this);
    }
  
    like() {
      console.log('like');
      this.setState({
        likes: this.state.likes + 1
      });
    }

    delete() {
      //console.log('delete')
      //console.log('index:', this.props.index, this.props)
      //debugger
      this.props.delete(this.props.index);
    }
    render() {
      return (
        <div className="status"> 
          <div className="close" onClick={this.delete}>X</div>
          <p>{this.props.myText}</p>
          <p><button onClick={this.like}>
            {this.state.likes} Likes
            </button>
          </p>
        </div>
      );
    }
  }